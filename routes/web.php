<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/biodata', 'AuthController@bio');
Route::post('/kirim', 'AuthController@kirim');

Route::get('/data-tabel', 'IndexController@tabel');

// CRUD kategori
//create
Route::get('/kategori/create', 'kategoriController@create'); //mengarah ke form tambah data
Route::post('/kategori', 'KategoriController@store'); //menyimpan data form ke database table kategori

//Read
Route::get('/kategori', 'kategoriController@index'); // ambil data ke data ditampilkan di blade
Route::get('/kategori/{kategori_id}', 'kategoriController@show'); // route detail kategori

//update
Route::get('/kategori/{kategori_id}/edit', 'kategoriController@edit'); // route untuk mengarah ke form edit
Route::put('/kategori/{kategori_id}', 'kategoriController@update'); // route untuk mengarah ke form edit

// delete
Route::delete('/kategori/{kategori_id}', 'kategoriController@destroy'); // route delete data berdasarkan id


Route::get('/master', function(){
    return view('page.home');
});
